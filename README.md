# usbnotify

usbnotify is used to display some of the meta attributes such ad idVendor, idProduct, manufacturer, product and filesystem as notification.

### Installation

#### Debian/Ubuntu

Download the deb binary from [here][download_deb]. Then to install

```sh
$ sudo apt install ./path/to/usbnotify_x.x-x.deb
```

This will install dependencies autoimatically.

#### Other Linux Distros

Clone the repo and install the dependencies using

```sh
$ sudo pip3 install pyudev notify2
```

Once the dependencies get installed 

```sh
$ sudo cp src/usbnotify /usr/bin/
$ sudo cp src/usbnotify_startup /etc/profile.d/
```

For first time runing use the following

```sh
$ usbnotify &
```

Note: this script starts automatically when a user get logged in.

Enjoy getting the notifications of the meta data of your pendrive on every time plugged in and out.

[download_deb]: https://gitlab.com/ragulkanth/usbnotify/blob/e4006f56746f2d65c90b01b83550bd445ad18d28/builds/usbnotify_0.1-1_all.deb
