Format: 3.0 (quilt)
Source: usbnotify
Binary: usbnotify
Architecture: all
Version: 0.1-1
Maintainer: USB Notify <ragul274@gmail.com>
Homepage: https://gitlab.com/ragulkanth/usbnotify
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 9)
Package-List:
 usbnotify deb unknown optional arch=all
Checksums-Sha1:
 09345684adc721849d935dacb50ff8b30bb55d9f 768 usbnotify_0.1.orig.tar.xz
 4fc69956fec9c87a637c2a587ff77157b5cba25c 8184 usbnotify_0.1-1.debian.tar.xz
Checksums-Sha256:
 a8735994e01fe76ceaf35c1e860b4f1249e56f6f15e1e217ce83cb870585259b 768 usbnotify_0.1.orig.tar.xz
 015014c90c1edba19d8799261c286edecf5c19b474864792a1a42df0c628c9ce 8184 usbnotify_0.1-1.debian.tar.xz
Files:
 6bb42b5be480d56a3fb2d05ad1d75f5d 768 usbnotify_0.1.orig.tar.xz
 11ef46e83adf9f713dcdf4430cd0cd0b 8184 usbnotify_0.1-1.debian.tar.xz
